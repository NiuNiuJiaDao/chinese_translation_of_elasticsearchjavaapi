# ElasticSearch-Java API      

## 目录
- [前言](docs/1Preface/readme.md)
- [Javadoc](docs/2JavaDoc/readme.md)
- [Maven仓库](docs/3MavenRepository/readme.md)
- [Client](docs/4Client/readme.md)
- [Document接口](docs/5DocumentAPIs/readme.md)
- [Search接口](docs/6SearchAPI/readme.md)
- [聚合](docs/7Aggregations/readme.md)
- [领域查询](docs/8QueryDSL/readme.md)
- [管理员接口](docs/9JavaAPIAdministration/readme.md)


- [《Elasticsearch: 权威指南》中文版](https://www.elastic.co/guide/cn/elasticsearch/guide/current/index.html)
- [名词解释(对比关系型数据库)](docs/0Remark/words.md)
- [贡献说明](docs/0Remark/Contribution.md)

> 原文[地址](https://www.elastic.co/guide/en/elasticsearch/reference/6.2/index.html)

> osc[地址](https://gitee.com/consolelog/chinese_translation_of_elasticsearchjavaapi)
> github[地址](https://github.com/qq253498229/ElasticSearchChineseGuide)

> 本文旨在帮助中文用户学习ES，转载请注明出处。

> 欢迎大家踊跃提交issue/pull request。联系方式：codeforfun@foxmail.com

> ES版本 6.2.2
